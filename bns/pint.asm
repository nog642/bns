section .data
    
c0 db "0"
c1 db "1"
c2 db "2"
c3 db "3"
c4 db "4"
c5 db "5"
c6 db "6"
c7 db "7"
c8 db "8"
c9 db "9"
newline db 0xd
x dd 0
dig dd 0
a dd 1
b dd 1


section .text
global _start

_start:

mov eax, dword[a]
push rax
call prnt_num
mov ebx, dword[b]
push rbx
call prnt_num

mov eax, dword[a]
mov ebx, dword[b]
add eax, ebx
add ebx, eax
mov dword[a], eax
mov dword[b], ebx

jmp _start

prnt_num:
pop rax
pop rbx
mov dword[x], ebx
push rax
mov dword[dig], 1
count_up:
mov eax, dword[dig]
mov ebx, 10
mul ebx
mov dword[dig], eax
cmp eax, dword[x]
jle count_up
prnt_dig:
xor edx, edx
mov eax, dword[dig]
mov ebx, 10
div ebx
mov dword[dig], eax
mov ebx, dword[dig]
mov eax, dword[x]
xor edx, edx
div ebx
xor edx, edx
mov ebx, 10
div ebx
push rdx
call print_char
mov eax, dword[dig]
cmp eax, 1
jne prnt_dig
mov rax, 4
mov rbx, 1
mov rcx, newline
mov rdx, 1
int 0x80
ret
print_char:
pop rax
pop r8
push rax
cmp r8, 0
jne not_0
call n0
ret
not_0:
cmp r8, 1
jne not_1
call n1
ret
not_1:
cmp r8, 2
jne not_2
call n2
ret
not_2:
cmp r8, 3
jne not_3
call n3
ret
not_3:
cmp r8, 4
jne not_4
call n4
ret
not_4:
cmp r8, 5
jne not_5
call n5
ret
not_5:
cmp r8, 6
jne not_6
call n6
ret
not_6:
cmp r8, 7
jne not_7
call n7
ret
not_7:
cmp r8, 8
jne not_8
call n8
ret
not_8:
cmp r8, 9
jne not_9
call n9
not_9:
ret
n0:
mov rax, 4
mov rbx, 1
mov rcx, c0
mov rdx, 1
int 0x80
ret
n1:
mov rax, 4
mov rbx, 1
mov rcx, c1
mov rdx, 1
int 0x80
ret
n2:
mov rax, 4
mov rbx, 1
mov rcx, c2
mov rdx, 1
int 0x80
ret
n3:
mov rax, 4
mov rbx, 1
mov rcx, c3
mov rdx, 1
int 0x80
ret
n4:
mov rax, 4
mov rbx, 1
mov rcx, c4
mov rdx, 1
int 0x80
ret
n5:
mov rax, 4
mov rbx, 1
mov rcx, c5
mov rdx, 1
int 0x80
ret
n6:
mov rax, 4
mov rbx, 1
mov rcx, c6
mov rdx, 1
int 0x80
ret
n7:
mov rax, 4
mov rbx, 1
mov rcx, c7
mov rdx, 1
int 0x80
ret
n8:
mov rax, 4
mov rbx, 1
mov rcx, c8
mov rdx, 1
int 0x80
ret
n9:
mov rax, 4
mov rbx, 1
mov rcx, c9
mov rdx, 1
int 0x80
ret

exit:

mov rax, 1
int 0x80

