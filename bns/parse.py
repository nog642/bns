#!/usr/bin/env python
import lib


OPERATORS = {
    '+': 1,
    '-': 1,
    '/': 2,
    '*': 2,
    '%': 2
}
ASCII = {
    '"': '0x22',
    "'": '0x27'
}


class Feed(object):

    def __init__(self, feed):
        self.feed = iter(feed)
        self.stack = []
        self.parens_to_add = 0

    def __iter__(self):
        return self

    def next(self):
        if self.stack:
            return self.stack.pop()
        else:
            val = next(self.feed)
            if self.parens_to_add and val == ')':
                self.parens_to_add -= 1
                self.stack.append(')')
            return val


class Token(object):

    def __init__(self, *args):
        self.args = args

    def __repr__(self):
        return '{}{}'.format(self.__class__.__name__, self.args)


class BinaryOperator(Token):

    def __init__(self, value, operands):
        super(BinaryOperator, self).__init__(value, operands)
        self.value = value
        self.operands = operands


class UnaryOperator(Token):

    def __init__(self, value, operand):
        super(UnaryOperator, self).__init__(value, operand)
        self.value = value
        self.operand = operand


class Print(Token):

    def __init__(self, operand):
        super(Print, self).__init__(operand)
        self.operand = operand


class Colog(Token):

    def __init__(self, operand):
        super(Colog, self).__init__(operand)
        self.operand = operand


class Assign(Token):

    def __init__(self, source, dest):
        super(Assign, self).__init__(source, dest)
        self.source = source
        self.dest = dest


class Var(Token):

    def __init__(self, name, vartype):
        super(Var, self).__init__(name, vartype)
        self.name = name
        self.vartype = vartype


class Literal(Token):

    def __init__(self, value, vartype):
        super(Literal, self).__init__(value, vartype)
        self.value = value
        self.vartype = vartype


class ReadChars(object):

    def __init__(self, f_in):
        self.f = open(f_in, 'r')
        self.stream = self._read()
        self.line = -1

    def __iter__(self):
        return self

    def next(self):
        try:
            return next(self.stream)
        except StopIteration:
            self.f.close()
            raise

    def _read(self):
        for line in self.f:
            self.line += 1
            for char in line.strip():
                yield char
        yield '}'


def tokenizer(feed):
    current_token = ''
    token_chars = ''
    string = ''
    escape = False
    for char in feed:
        if string and char == '\\':
            escape = True
        if escape:
            if char == string:
                current_token += char
            escape = False
        if char not in token_chars:
            str_flag = False
            if string:
                current_token += string
                string = ''
                str_flag = True
            if current_token:
                yield current_token
            current_token = ''
            token_chars = ''
            if str_flag:
                continue
        if not token_chars:
            if char in lib.ALPHA:
                token_chars = lib.ALPHANUM
            elif char in lib.NUM + '.':
                token_chars = lib.NUM + '.'
            elif char in OPERATORS.keys() + ['=']:
                token_chars = char
            elif char in (';', '{', '}', '(', ')', '[', ']', ','):
                token_chars = ''
                current_token += char
            elif char in ('"', "'"):
                token_chars = lib.ALPHANUM + '~`!@#$%^&*()-_=+[]{}\\|;:<>?,./ ' + {"'": '"', '"': "'"}[char]
                current_token += char
                string = char
        if char in token_chars:
            current_token += char
    yield current_token


def parse_block(feed):
    block = []
    for token in feed:
        if token == 'print':
            block.append(parse_print(feed))
        elif token == 'colog':
            block.append(parse_colog(feed))
        elif token == '}':
            return block
        else:
            raise Exception(token)
    raise SyntaxError('unfinished code block')


def parse_exp_statement(feed, token_class):
    def exp():
        for token in feed:
            if token == ';':
                break
            yield token
        yield ')'
    exp_gen = Feed(exp())
    exp_val = parse_exp(exp_gen, 0)[0]
    for _ in exp_gen:
        raise SyntaxError("invalid syntax")
    return token_class(exp_val)


def parse_print(feed):
    return parse_exp_statement(feed, Print)


def parse_colog(feed):
    return parse_exp_statement(feed, Colog)


def parse_exp(exp, precedence):
    expression = []

    def handle_operator(tok):
        if expression:
            prev = expression.pop()
            if expression:
                raise Exception(expression)
            if OPERATORS[tok] <= precedence:
                return prev, tok
            exp.parens_to_add += 1
            after, next_op = parse_exp(exp, OPERATORS[tok])
            expression.append(BinaryOperator(tok, (prev, after)))
            if next_op is not None:
                handle_operator(next_op)
        else:
            if tok not in ('-',):
                raise SyntaxError("unknown unary operator: '{}'".format(tok))
            # print 'haha lol'


            # if OPERATORS[tok] <= precedence:
            #     return prev, tok
            exp.parens_to_add += 1
            after, next_op = parse_exp(exp, OPERATORS[tok])
            expression.append(UnaryOperator(tok, after))
            if next_op is not None:
                handle_operator(next_op)

    for token in exp:
        if token[0] in ('"', "'"):
            string = token[0]
            expression.append(Literal(
                token.replace(
                    '\\\\',
                    '\\'
                ).replace(
                    '\\n',
                    '{0}, 0xa, {0}'.format(string)
                ).replace(
                    '\\{}'.format(string),
                    '{0}, {1}, {0}'.format(string, ASCII[string])
                ),
                'str'
            ))
        elif token[0] in lib.NUM + '.':
            if '.' in token:
                expression.append(Literal(
                    token, 'float'
                ))
            else:
                expression.append(Literal(
                    token, 'int'
                ))
        elif token in OPERATORS:
            retval = handle_operator(token)
            if retval is not None:
                return retval
        elif token == '(':
            expression.append(parse_exp(exp, 0)[0])
        elif token == ')':
            if not expression:
                raise SyntaxError('invalid syntax')
            return expression[0], None
        elif token[0] in lib.ALPHA:
            expression.append(Var(token, '???'))
        else:
            raise SyntaxError('unknown token: ' + token)
    raise SyntaxError("expected ')', not found")


def parser(feed):
    token_feed = tokenizer(feed)
    return parse_block(token_feed)
