section .data
EXIT_SUCCESS equ 0
SYS_EXIT equ 60
var_x db "ok"
var_len_x equ $-var_x
print_a db "al", 0x0a
print_len_a equ $-print_a
section .text
global _start
_start:
mov eax, 4
mov ebx, 1
mov ecx, print_a
mov edx, print_len_a
int 0x80
mov rax, SYS_EXIT
mov rdi, EXIT_SUCCESS
syscall
