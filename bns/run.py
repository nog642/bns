#!/usr/bin/env python
import argparse
import subprocess
import sys


def run(command):
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if output:
        print output.strip()
    if error:
        print error.strip()
        sys.exit(-1)


def main(asm, mode, rem):
    if not asm.endswith(".asm"):
        raise ValueError("file must end with '.asm'")
    name = asm[:-4]
    run("yasm -g dwarf2 -f elf64 {} -l {}.lst".format(asm, name))
    if mode == 'gcc':
        run("gcc -s -o {0} {0}.o".format(name))
    else:
        run("ld -g -o {0} {0}.o".format(name))
    run("rm {}.o".format(name))
    run("./{}".format(name))
    if rem:
        run("rm {}".format(name))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("asm")
    parser.add_argument("--gcc", '-g', default=0, required=False, type=int)
    parser.add_argument("--rem", '-r', default=1, required=False, type=int)
    args = parser.parse_args()
    main(args.asm, ['ld', 'gcc'][args.gcc], bool(args.rem))
