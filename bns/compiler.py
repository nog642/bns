#!/usr/bin/env python
import argparse
import lib
import parse


def evaluate(exp):
    if isinstance(exp, parse.Colog):
        print evaluate(exp.operand)
    elif isinstance(exp, parse.BinaryOperator):
        return eval('{}{}{}'.format(evaluate(exp.operands[0]), exp.value, evaluate(exp.operands[1])))
    elif isinstance(exp, parse.Literal):
        return eval(exp.value)
    elif isinstance(exp, parse.UnaryOperator):
        return exp


def main(f_in, f_out):
    char_stream = parse.ReadChars(f_in)
    try:
        for s in parse.parser(char_stream):
            evaluate(s)
    except SyntaxError as e:
        print 'line {} in {}:'.format(char_stream.line, f_in)
        print next(lib.get_lines(f_in, (char_stream.line,))).strip()
        print 'syntax error: {}'.format(e)


if __name__ == '__main__':
    argparser = argparse.ArgumentParser()
    argparser.add_argument('file')
    argparser.add_argument('--out', '-o')
    # argparser.add_argument('--debug', '-d', required=False, default=0, type=int)
    arguments = argparser.parse_args()
    main(arguments.file, arguments.out)
