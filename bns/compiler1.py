#!/usr/bin/env python
import argparse
import lib
OPERATORS = {'='}
START_VARCHARS = lib.ALPHA + '$_'
VARCHARS = lib.ALPHANUM + '$_'


class BnsCompileError(Exception):

    def __init__(self, message=''):
        self.message = message

    def __str__(self):
        return '{}: {}'.format(type(self).__name__, self.message)


class BnsSyntaxError(BnsCompileError):
    pass


class BnsUndefinedError(BnsCompileError):
    pass


class BnsCharParseError(Exception):
    pass


class WorkingError(Exception):
    pass


def clean_varname(name):
    return lib.replace_all(name, {
        '0': '__zero__',
        '1': '__one__',
        '2': '__two__',
        '3': '__three__',
        '4': '__four__',
        '5': '__five__',
        '6': '__six__',
        '7': '__seven__',
        '8': '__eight__',
        '9': '__nine__',
        '?': '__qmark__'
    })


def bns_set_var(data_section, text_section, symtab, name, val):
    if name in symtab:
        # TODO var_len_{} has not been changed yet. it must be.
        text_section.append('mov byte[var_{}], {}'.format(symtab[name], bns_eval(data_section, text_section, val)['val']))
    else:
        asm_name = clean_varname(name)
        val = bns_eval(symtab, val)
        data_section.append('var_{} db {}'.format(asm_name, val['val']))
        data_section.append('var_len_{0} equ $-var_{0}'.format(asm_name))
        symtab[name] = {'val': asm_name, 'type': {'str_lit': 'str_var'}[val['type']]}


def bns_eval(symtab, exp):
    if type(exp) is dict:
        return exp
    if len(exp) == 1:
        exp = exp[0]
        if type(exp) is dict:
            return exp
        if exp[0] in START_VARCHARS and all(exp[i] in VARCHARS for i in xrange(len(exp))):
            if exp in symtab:
                return symtab[exp]
            else:
                raise BnsUndefinedError('variable {} referenced before assignment'.format(exp))
    elif exp[0] == 'str' and len(exp) == 2 and type(exp[1]) is list:
        exp = bns_eval(symtab, exp[1])
        if exp['type'] == 'str_lit':
            return exp
        elif exp['type'] == 'str_var':
            return exp
        else:
            return "string conversion not implemented"


def bns_print(data_section, text_section, symtab, i_counter, exp):
    exp = bns_eval(symtab, exp)
    if exp['type'] == 'str_lit':
        i = next(i_counter)
        msg_name = 'print_{}'.format(i)
        len_name = 'print_len_{}'.format(i)
        data_section.append('{} db {}, 0x0a'.format(msg_name, exp['val']))
        data_section.append('{} equ $-{}'.format(len_name, msg_name))
    elif exp['type'] == 'str_var':
        msg_name = 'var_{}'.format(exp['val'])
        len_name = 'var_len_{}'.format(exp['val'])
    else:
        print exp
        raise WorkingError
    text_section.append('mov eax, 4')
    text_section.append('mov ebx, 1')
    text_section.append('mov ecx, {}'.format(msg_name))
    text_section.append('mov edx, {}'.format(len_name))
    text_section.append('int 0x80')


def bns_colog(data_section, text_section, symtab, i_counter, exp):
    i = next(i_counter)
    data_section.append('print_{} db {}'.format(i, bns_eval(symtab, exp)))
    data_section.append('print_len_{0} equ $-print_{0}'.format(i))
    text_section.append('mov eax, 4')
    text_section.append('mov ebx, 1')
    text_section.append('mov ecx, print_{}'.format(i))
    text_section.append('mov edx, print_len_{}'.format(i))
    text_section.append('int 0x80'.format(i))


def process_char(char, substatement, invar, instr, var, string, parens, str_esc, quotes):
    if invar:
        if char not in VARCHARS:
            invar = False
            substatement.append(var)
            var = ''
            substatement, invar, instr, var, string, parens, str_esc, quotes = process_char(
                char,
                substatement,
                invar,
                instr,
                var,
                string,
                parens,
                str_esc,
                quotes
            )
        else:
            var += char
    elif instr:
        if str_esc:
            if char == 'n':
                string += '", 0x0a, "'
            elif char == '\\':
                string += '\\'
            elif char == quotes:
                if quotes == '"':
                    string += '", 0x22, "'
                else:
                    string += "'"
            else:
                string += '\\' + char
            str_esc = False
        elif char == quotes:
            instr = False
            quotes = None
            substatement.append({'val': '"{}"'.format(string.replace(', ""', '')), 'type': 'str_lit'})
            string = ''
        elif char == '"':
            string += '", 0x22, "'
        else:
            if char == '\\':
                str_esc = True
            else:
                string += char
    else:
        if char == '"':
            quotes = '"'
        elif char == "'":
            quotes = "'"
        elif char in START_VARCHARS:
            invar = True
            var += char
        elif char in OPERATORS:
            substatement.append(char)
        elif char == ' ':
            pass
        elif char == '(':
            substatement.append([])
            parens += 1
        elif char == ')':
            parens -= 1
        else:
            raise BnsCharParseError()
        if quotes:
            instr = True
    return substatement, invar, instr, var, string, parens, str_esc, quotes


def parse_line(line):
    statement = []
    invar = False
    instr = False
    parens = 0
    var = ''
    string = ''
    str_esc = False
    quotes = None
    for char in line:
        popen = bool(parens)
        try:
            substatement, invar, instr, var, string, parens, str_esc, quotes = process_char(
                char,
                eval('statement{}'.format('[-1]' * parens)),
                invar,
                instr,
                var,
                string,
                parens,
                str_esc,
                quotes
            )
        except BnsCharParseError as e:
            if str(e):
                raise BnsSyntaxError("{}: {}".format(e, line))
            else:
                raise BnsSyntaxError("invalid statement: {}".format(line))
        exec 'statement{} = substatement{}'.format('[-1]' * (parens - (not popen)), '[-1]' * -(bool(parens) & popen))
    if invar:
        statement.append(var)
    elif instr:
        statement.append(string)
    return statement


def main(f_in, f_out):
    data_section = []
    text_section = []
    bns_print_i = lib.alpha_count()
    symtab = {}
    with open(f_in, 'r') as f:
        for line in [x.strip() for x in f.read().split(';') if x.strip()]:
            statement = parse_line(line)
            if statement[0] == 'print':
                bns_print(data_section, text_section, symtab, bns_print_i, statement[1:])
            elif statement[0] == 'colog':
                bns_colog(data_section, text_section, bns_print_i, statement[1:])
            elif '=' in statement:
                try:
                    name, val = lib.split_list(statement, '=')
                except ValueError:
                    raise BnsSyntaxError('invalid assignment: {}'.format(line))
                if len(name) > 1:
                    raise BnsSyntaxError('can only assign to variable name: {}'.format(line))
                bns_set_var(data_section, text_section, symtab, name[0], val)
    with open(f_out, 'w') as f:
        f.write('section .data\n')
        f.write('EXIT_SUCCESS equ 0\n')
        f.write('SYS_EXIT equ 60\n')
        for line in data_section:
            f.write(line + '\n')
        f.write('section .text\n')
        f.write('global _start\n')
        f.write('_start:\n')
        for line in text_section:
            f.write(line + '\n')
        f.write('mov rax, SYS_EXIT\n')
        f.write('mov rdi, EXIT_SUCCESS\n')
        f.write('syscall\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    parser.add_argument('--out', '-o')
    parser.add_argument('--debug', '-d', required=False, default=0, type=int)
    args = parser.parse_args()
    try:
        main(args.file, args.out)
    except BnsSyntaxError as e:
        if args.debug:
            raise
        else:
            print e
