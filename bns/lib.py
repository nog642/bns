#!/usr/bin/env python
import itertools


ALPHA = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
NUM = '0123456789'
ALPHANUM = ALPHA + NUM


def replace_all(s, d):
    for k in d:
        s = s.replace(k, d[k])
    return s


def get_lines(f_in, lines):
    with open(f_in, 'r') as f:
        for i, line in enumerate(f):
            if i in lines:
                yield line

def alpha_count():
    for length in itertools.count(1):
        for chars in itertools.product(ALPHA, repeat=length):
            yield ''.join(chars)

