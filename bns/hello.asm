section .data
    
s db "Hello World"
len equ $-s

section .text

global _start
    
_start:

mov eax, 4
mov ebx, 1
mov ecx, s
mov edx, len
int 0x80

mov rax, 1
int 0x80

